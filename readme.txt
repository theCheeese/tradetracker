This is just a tool for me to approach trading in a more structured way and to test the performance of certain strategies. 
It's also an exercise in writing Go code.
It assumes you have postgresql installed and set up according to your config.go. 
This includes running "create database", but the schema can be created by running the executable and using the init command.
