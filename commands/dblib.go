package commands

import (
    "fmt"
    "database/sql"
    _ "github.com/lib/pq"
)

//connection string variables defined in config.go
func ConnectToDb() (*sql.DB, error) {
    connectionString := fmt.Sprintf("host='%s' port='%s' user='%s' password='%s' dbname='%s' sslmode=disable", host, port, user, password, dbname)
    db, err := sql.Open("postgres", connectionString)
    if(err != nil) {
        return nil, err
    }

    return db, nil
}
