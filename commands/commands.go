package commands

import (
    "fmt"
    _ "database/sql"
    "github.com/spf13/cobra"
    _ "github.com/lib/pq"
)

var RootCmd = &cobra.Command{
    Use: "tradetracker",
    Run: func(cmd *cobra.Command, args []string) {
        fmt.Println("Hello World")
    },
}

var writeCmd = &cobra.Command{
    Use: "write",
    Run: write,
}

var reportCmd = &cobra.Command{
    Use: "report",
    Run: report,
}

var pingCmd = &cobra.Command{
    Use: "ping",
    Run: ping,
}

var initCmd = &cobra.Command{
    Use: "init",
    Run: initDb,
}

func InitCommands() {
    RootCmd.AddCommand(writeCmd)
    RootCmd.AddCommand(reportCmd)
    RootCmd.AddCommand(pingCmd)
    RootCmd.AddCommand(initCmd)
}

func write(cmd *cobra.Command, args []string) {
    fmt.Println("Executing Write Command")
}

func report(cmd *cobra.Command, args []string) {
    fmt.Println("Executing Report Command")
}

//consider making the database connection in a separate connection
func ping(cmd *cobra.Command, args []string) {
    db, err := ConnectToDb()
    if err != nil {
        panic(err)
    }
    defer db.Close()

    err = db.Ping()
    if err != nil {
        panic(err)
    }

    fmt.Println("ok")
}

func initDb(cmd *cobra.Command, args []string) {
    //connect
    fmt.Println("Connecting...")
    db, connErr := ConnectToDb()
    if connErr != nil {
        fmt.Println("Fatal: failed to connect to db.")
        panic(connErr)
    }
    defer db.Close()

    sql := `CREATE TABLE IF NOT EXISTS TradeType (
               ID serial PRIMARY KEY,
               Description varchar(20)
           );
           CREATE TABLE IF NOT EXISTS ProductType (
               ID serial PRIMARY KEY,
               Description varchar(20)
           ); 
           CREATE TABLE IF NOT EXISTS Trade (
               ID serial PRIMARY KEY,
               TradeTypeID INT REFERENCES TradeType(ID),
               BuyingPowerReduction money NOT NULL,
               ProductType INT REFERENCES ProductType(ID),
               OpeningTradeID INT REFERENCES Trade(ID)
           );`
    _, err := db.Exec(sql)
    if err != nil {
        fmt.Println("Fatal: Could not create schema.")
        panic(err)
    } else {
        fmt.Println("Created tables TradeType, ProductType, Trade.")
    }


    fmt.Println("ok")
}
