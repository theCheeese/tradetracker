package main

import (
    "fmt"
    "os/exec"
    "syscall"
    "tradetracker/commands"
)

func main() {
    checkPostgres := exec.Command("which", "-s", "psql")
    if err := checkPostgres.Start(); err != nil {
        fmt.Println("Fatal: could not execute shell command which -s psql")
        return
    }

    if err := checkPostgres.Wait(); err != nil {
        if exitCode, ok := err.(*exec.ExitError); ok {
            if _, ok := exitCode.Sys().(syscall.WaitStatus); ok {
                fmt.Println("Fatal: postgresql not found. If it is installed, check your $PATH")
                return
            }
        }
    }

    commands.InitCommands()
    commands.RootCmd.Execute()
}
